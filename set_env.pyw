import os
import sys
import json

if __name__ == '__main__':

    # get dirs
    fileDir = os.path.dirname(__file__)
    envDir = os.path.dirname(sys.executable)
    settingsPath = '.vscode\\settings.json'

    # read setting.json
    try:
        with open(settingsPath, 'r') as file:            
            settings = json.load(file)
            file.close()
    except:
        settings = {}

    # set path
    if 'python.autoComplete.extraPaths' in settings:
        paths = settings['python.autoComplete.extraPaths']
    else:
        paths = []
    settings['python.autoComplete.extraPaths'] = list(set(paths + [
        os.path.join(envDir, 'Library\\python'),
        os.path.join(envDir, 'Library\\python\\plugins')
    ]))

    # set qt extension
    settings['pyqt-integration.linguist.cmd'] = os.path.join(
        envDir, 'Library\\bin\\linguist')
    settings['pyqt-integration.pylupdate.cmd'] = os.path.join(
        envDir, 'Library\\bin\\pylupdate5')
    settings['pyqt-integration.qtdesigner.path'] = os.path.join(
        envDir, 'Library\\bin\\designer')
    settings['pyqt-integration.pyuic.cmd'] = os.path.join(envDir, 'pyuic5')
    settings['pyqt-integration.pyrcc.cmd'] = os.path.join(envDir, 'pyrcc5')

    # write setting.json
    with open(settingsPath, 'w') as file:
        json.dump(settings, file, indent=4)
        file.close()
