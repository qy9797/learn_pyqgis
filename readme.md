# Python GIS 开发说明
## 环境配置

conda环境 安装包：https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe

安装环境：conda env create -f dev.gis.scrs.yml

更新环境：conda env update -f dev.gis.scrs.yml

mamba环境 安装包：https://github.com/conda-forge/miniforge/releases/download/23.11.0-0/Mambaforge-23.11.0-0-Windows-x86_64.exe

安装环境：mamba env create -f dev.gis.scrs.yml

更新环境：mamba env update -f dev.gis.scrs.yml

## 配置qgis的vscode开发环境

在vscode编辑器中qgis的引入会报错，请按一下步骤解决：

1. 在vscode中，按"ctrl+,"

1. 搜索"python.autoComplete.extraPaths"

1. 点击在"setting.json"

1. 在"python.autoComplete.extraPaths"中添加
```
    "your_conda_path\\envs\\dev.gis.scrs\\Library\\python",
    "your_conda_path\\envs\\dev.gis.scrs\\Library\\python\\plugins"
    （"your_conda_path"替换成你计算机上的路径）
```
完成后重启vscode即可

## 代码自动整理插件

建议使用autopep8(在vs中使用extension安装)

在VSCode中，代码编写完成后，使用Alt+Shift+F自动整理代码

## 环境包中QGIS和QtDesigner的使用

QGIS路径："your_conda_path\envs\dev.gis.scrs\Library\bin\qgis.exe"

QtDesigner路径："your_conda_path\envs\dev.gis.scrs\Library\bin\designer.exe"