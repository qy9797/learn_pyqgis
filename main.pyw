
import os
import sys
try:
    from qgis.PyQt import QtCore
    from qgis.core import QgsApplication
    from PyQt5.QtCore import Qt
except:
    os.path.dirname(sys.executable)
    sys.path.append(os.path.join(os.path.dirname(
        sys.executable), 'Library\\python'))
    sys.path.append(os.path.join(os.path.dirname(
        sys.executable), 'Library\\python\\plugins'))
    from qgis.PyQt import QtCore
    from qgis.core import QgsApplication
    from PyQt5.QtCore import Qt

from mainWindow import MainWindow

if __name__ == '__main__':
    # enviroDir = r"C:\qgis322"
    # os.environ['GDAL_DATA'] = os.path.join(enviroDir, 'share', 'gdal')
    # proj lib
    # os.environ['PROJ_LIB'] = os.path.join(enviroDir, 'share', 'proj')
    # geotiff_csv
    # os.environ['GEOTIFF_CSV'] = os.path.join(enviroDir, 'share', 'epsg_csv')
    # QgsApplication.prefixPath()
    # QgsApplication.setPrefixPath(os.path.join(enviroDir, "apps/qgis-ltr"), True)
    QgsApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    app = QgsApplication([], True)

    # 中文翻译软件
    t = QtCore.QTranslator()
    t.load(r'.\zh-Hans.qm')
    app.installTranslator(t)

    app.initQgis()

    mainWindow = MainWindow()
    mainWindow.show()
    # shp = r"D:\111.shp"
    # tif = r"D:\test.tif"
    # mainWindow.addVectorLayer(shp)
    # mainWindow.addRasterLayer(tif)

    app.exec_()
    app.exitQgis()
